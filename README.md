# ipmiComm conda recipe

Home: https://github.com/icshwi/ipmiComm

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS ipmiComm module
